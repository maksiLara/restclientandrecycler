package com.newsapi.larchanka.queringandresponcejson.apiClient;

import com.newsapi.larchanka.queringandresponcejson.dataNews.Articles;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NewsApi {

    @GET("top-headlines")
    Call<Articles> topHeadlines(
            @Query("county") String county,
            @Query("category") String category
    );
}
