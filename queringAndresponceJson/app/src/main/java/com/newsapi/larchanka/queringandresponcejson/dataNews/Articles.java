package com.newsapi.larchanka.queringandresponcejson.dataNews;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Articles {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("totalResults")
    @Expose
    private int totalResults;
    @SerializedName("articles")
    @Expose
    private ArrayList<Artcile> articles = new ArrayList<>();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Articles withStatus(String status) {
        this.status = status;
        return this;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
    }

    public Articles withTotalResults(int totalResults) {
        this.totalResults = totalResults;
        return this;
    }

    public ArrayList<Artcile> getArticles() {
        return articles;
    }

    public void setArticles(ArrayList<Artcile> articles) {
        this.articles = articles;
    }

    public Articles withArticles(ArrayList<Artcile> articles) {
        this.articles = articles;
        return this;
    }

}