package com.newsapi.larchanka.queringandresponcejson;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.ScrollingTabContainerView;
import android.telecom.Call;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toolbar;

import com.newsapi.larchanka.queringandresponcejson.adapter.RecyclerNewsAdapter;
import com.newsapi.larchanka.queringandresponcejson.apiClient.NewsApi;
import com.newsapi.larchanka.queringandresponcejson.apiClient.RestClient;
import com.newsapi.larchanka.queringandresponcejson.dataNews.Artcile;
import com.newsapi.larchanka.queringandresponcejson.dataNews.Articles;
import com.newsapi.larchanka.queringandresponcejson.dataNews.Source;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity {
    final String MY_LOG = "MyLog";

    private ListView listView;
    private View view;

    private ArrayList<Articles> articlesList;
    private RecyclerNewsAdapter recyclerNewsAdapter;
    Toolbar toolbar;

    /*
        RecyclerNewsAdapter recyclerNewsAdapter;
        RestClient restClient;

        @BindView(R.id.findText)
        EditText editText;


        @BindView(R.id.recycleView)
        public RecyclerView recyclerView;
    */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        articlesList = new ArrayList<>();
        view = findViewById(R.id.parentLayout);

        listView = findViewById(R.id.listView);
        listView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Snackbar.make(view, articlesList.get(position)
                        .getArticles()
                        .get(position).getAuthor()
                        + "!!!!!!"
                        + articlesList.get(position)
                        .getArticles()
                        .get(position)
                        .getDescription(), Snackbar.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        assert fab != null;
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(@NonNull View v) {
                if (InternetConnection.chekConnection(getApplicationContext())) {
                    final ProgressDialog progressDialog;
                    progressDialog = new ProgressDialog(MainActivity.this);
                    progressDialog.show();

                    NewsApi newsApi = RestClient.getNewsApi();
                    final retrofit2.Call<Articles> articlesCall = newsApi.topHeadlines("us", "bussines");

                    articlesCall.enqueue(new Callback<Articles>() {
                        @Override
                        public void onResponse(retrofit2.Call<Articles> call, Response<Articles> response) {
                            progressDialog.dismiss();
                            if (response.isSuccessful()) {
                                articlesList = response.body().getArticles();
                                recyclerNewsAdapter = new RecyclerNewsAdapter(MainActivity.this, articlesList);
                                listView.setAdapter(recyclerNewsAdapter);
                            } else {
                                Snackbar.make(view, "WRONG", Snackbar.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(retrofit2.Call<Articles> call, Throwable t) {
                            progressDialog.dismiss();
                        }


                       /* @Override
                        public void onFailure(retrofit2.Call<Artcile> call, Throwable t) {

                        }*/
                    });



                } else {
                    Snackbar.make(view, "NO NO NO", Snackbar.LENGTH_LONG).show();
                }
            }
        });




   /*     Intent intent = new Intent(MainActivity.this, RestClient.class);
        startService(intent);
        ButterKnife.bind(this);

        restClient = new RestClient();
        recyclerViewSetup(recyclerView);

    }

    private void recyclerViewSetup(RecyclerView recyclerView) {
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));*/
    }



}



